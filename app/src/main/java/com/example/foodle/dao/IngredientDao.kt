package com.example.foodle.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.foodle.models.Ingredient

@Dao
interface IngredientDao {
    @Insert
    fun insert(ing: Ingredient)

    @Insert
    fun insertAll(vararg ings: Ingredient)

    @Delete
    fun delete(ing: Ingredient)

    @Query("DELETE FROM Ingredient WHERE ingredient_name LIKE :name")
    fun deleteIngsByName(name: String)

    @Query("SELECT * FROM Ingredient")
    fun getAll(): List<Ingredient>

    @Query("SELECT * FROM Ingredient WHERE ingredient_name LIKE :name")
    fun findIngsByName(name: String): Array<Ingredient>
}