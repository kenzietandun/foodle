package com.example.foodle

import com.example.foodle.models.Ingredient
import com.example.foodle.models.Recipe

object TestUtil {
    fun createIngredient(name: String) = Ingredient(
        id = 0,
        ingName = name,
        stockAmount = 0
    )

    fun createMultipleIngredients(fstIng: String, sndIng: String) : Array<Ingredient> {
        var ing1 = Ingredient(
            id = 1,
            ingName = fstIng,
            stockAmount = 10
        )

        var ing2 = Ingredient(
            id = 2,
            ingName = sndIng,
            stockAmount = 20
        )

        return arrayOf(ing1, ing2)
    }

    fun createRecipe(name: String) = Recipe(
        id = 0,
        recipeName = name
    )
}
