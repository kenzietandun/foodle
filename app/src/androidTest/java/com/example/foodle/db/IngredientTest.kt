package com.example.foodle.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.foodle.TestUtil
import com.example.foodle.dao.IngredientDao
import com.example.foodle.models.Ingredient
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@SmallTest
class SimpleIngredientTest {
    private lateinit var ingDao: IngredientDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        ingDao = db.getIngDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeIngredientAndReadInList() {
        val ing: Ingredient = TestUtil.createIngredient("Leek")
        ing.stockAmount = 10

        ingDao.insert(ing)
        val byName = ingDao.findIngsByName("Leek")
        assertThat(byName.get(0).ingName, equalTo(ing.ingName))
        assertThat(byName.get(0).stockAmount, equalTo(ing.stockAmount))
    }

    @Test
    @Throws(Exception::class)
    fun writeMultipleIngredientsAndReadInList() {
        val ingredients: Array<Ingredient> =
            TestUtil.createMultipleIngredients("Lettuce", "Tomato")
        ingDao.insertAll(*ingredients)

        val byNameLettuce = ingDao.findIngsByName("Lettuce")
        val byNameTomato = ingDao.findIngsByName("Tomato")

        assertEquals("Lettuce", byNameLettuce.get(0).ingName)
        assertEquals("Tomato", byNameTomato.get(0).ingName)
    }

    @Test
    @Throws(Exception::class)
    fun writeIngredientAndDelete() {
        val ingName = "Chicken Breast"
        val ing: Ingredient = TestUtil.createIngredient(ingName)
        ingDao.insert(ing)

        var byName = ingDao.findIngsByName(ingName)
        assertEquals(ingName, byName.get(0).ingName)

        ingDao.delete(byName.get(0))
        byName = ingDao.findIngsByName(ingName)
        assertEquals(byName.size, 0)
    }

    @Test
    @Throws(Exception::class)
    fun writeAndDeleteIngredientByName() {
        val ingName = "Chives"
        val ing: Ingredient = TestUtil.createIngredient(ingName)
        ingDao.insert(ing)

        ingDao.deleteIngsByName("Not Chives")
        var byName = ingDao.findIngsByName(ingName) // chives should still in DB
        assertNotEquals(byName.size, 0)

        ingDao.deleteIngsByName(ingName)
        byName = ingDao.findIngsByName(ingName)
        assertEquals(byName.size, 0)
    }

}
