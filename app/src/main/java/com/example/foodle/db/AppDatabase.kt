package com.example.foodle.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.foodle.dao.IngredientDao
import com.example.foodle.dao.IngredientRecipeJoinDao
import com.example.foodle.dao.RecipeDao
import com.example.foodle.models.Ingredient
import com.example.foodle.models.IngredientRecipeJoin
import com.example.foodle.models.Recipe

@Database(entities = arrayOf(
    Ingredient::class,
    Recipe::class,
    IngredientRecipeJoin::class
    ), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getIngDao(): IngredientDao
    abstract fun getRecipeDao(): RecipeDao
    abstract fun getIngredientRecipeDao(): IngredientRecipeJoinDao
}
