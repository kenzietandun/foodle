package com.example.foodle.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.foodle.TestUtil
import com.example.foodle.dao.RecipeDao
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@SmallTest
class SimpleRecipeTest {
    private lateinit var recDao: RecipeDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java).build()
        recDao = db.getRecipeDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeRecipeAndRead() {
        val recipe = TestUtil.createRecipe("Omuraisu")
        recDao.insert(recipe)

        val byName = recDao.findRecipesByName("Omuraisu")
        assertEquals(byName.get(0).recipeName, recipe.recipeName)
    }

    @Test
    @Throws(Exception::class)
    fun writeEmptyRecipeNameShouldFail() {
        val recipe = TestUtil.createRecipe("")
        recDao.insert(recipe)
    }
}
