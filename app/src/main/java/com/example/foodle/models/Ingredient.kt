package com.example.foodle.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Ingredient(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "ingredient_name") var ingName: String,
    @ColumnInfo(name = "stock_amount") var stockAmount: Int
)