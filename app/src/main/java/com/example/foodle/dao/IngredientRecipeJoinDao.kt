package com.example.foodle.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.foodle.models.Ingredient
import com.example.foodle.models.IngredientRecipeJoin
import com.example.foodle.models.Recipe

@Dao
interface IngredientRecipeJoinDao {
    @Insert
    fun insert(ingredientRecipeJoin: IngredientRecipeJoin)

    @Query("""
        SELECT * 
        FROM ingredient INNER JOIN ingredient_recipe_join
            ON ingredient.id = ingredient_id
        WHERE ingredient_recipe_join.recipe_id = :recipeId
            """)
    fun getIngredientsForRecipe(recipeId: Int): Array<Ingredient>

    @Query("""
        SELECT * 
        FROM recipe INNER JOIN ingredient_recipe_join
            ON recipe.id = recipe_id
        WHERE ingredient_recipe_join.ingredient_id = :ingredientId
            """)
    fun getRecipesForIngredient(ingredientId: Int): Array<Recipe>
}