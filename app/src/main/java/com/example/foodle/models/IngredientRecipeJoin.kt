package com.example.foodle.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(tableName = "ingredient_recipe_join",
        primaryKeys = arrayOf("ingredient_id", "recipe_id"),
        foreignKeys = arrayOf(
                ForeignKey(
                        entity = Ingredient::class,
                        parentColumns = arrayOf("id"),
                        childColumns = arrayOf("ingredient_id")
                        ),
                ForeignKey(
                        entity = Recipe::class,
                        parentColumns = arrayOf("id"),
                        childColumns = arrayOf("recipe_id")
                )
        ))
data class IngredientRecipeJoin(
    @ColumnInfo(index = true, name = "ingredient_id") val ingredientId: Int,
    @ColumnInfo(index = true, name = "recipe_id") val recipeId: Int,
    @ColumnInfo(name = "amount") val amountNeeded: Int
)