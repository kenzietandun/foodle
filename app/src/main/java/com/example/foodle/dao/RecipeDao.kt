package com.example.foodle.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.foodle.models.Recipe

@Dao
interface RecipeDao {
    @Insert
    fun insert(recipe: Recipe)

    @Delete
    fun delete(recipe: Recipe)

    @Query("DELETE FROM Recipe WHERE recipe_name LIKE :name")
    fun deleteRecipeByName(name: String)

    @Query("SELECT * FROM Recipe")
    fun getAll(): List<Recipe>

    @Query("SELECT * FROM Recipe WHERE recipe_name LIKE :name")
    fun findRecipesByName(name: String): Array<Recipe>
}